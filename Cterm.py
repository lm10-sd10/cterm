import wx
import os
import re
import os.path
	
class pan(wx.Panel):
	def __init__(self,parent):
		super().__init__(parent)
		self.parent=parent
		self.frame=parent
		self.mainsizer=wx.BoxSizer(wx.VERTICAL)
		self.subsizer=wx.BoxSizer(wx.HORIZONTAL)
		#Choice Maker...
		languages = ['Please Choose a File Format','C','C_With_GTK2','C_With_GTK3','Rust_Project','C++','Flex', 'Python','Java', 'PHP','x86_Assembly'
		,'Unix_Shell_Script']
		self.choice=wx.Choice(self,choices=languages)
		self.choice.SetSelection(0)
		self.mainsizer.Add(self.choice,0,wx.EXPAND|wx.ALL,5)
		#Buttonsssssss
		new=wx.Button(self,label="New")
		about=wx.Button(self,label="About")
		self.subsizer.Add(new,1,wx.ALIGN_LEFT|wx.TOP|wx.LEFT,10)
		self.subsizer.AddStretchSpacer(1)
		self.subsizer.Add(about,1,wx.ALIGN_RIGHT|wx.TOP|wx.RIGHT,10)
		self.mainsizer.Add(self.subsizer,0,wx.EXPAND)		
#Final   Modifications  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		self.Bind(wx.EVT_BUTTON,self.click)
		self.SetSizer(self.mainsizer)	
	def click(self,event):
		bu=event.EventObject
		if (bu.Label=="About") :
			wx.MessageBox("""Thank You For Using....
			Cterm 
			                 By
			                         SD
If you find any bugs then Contact Me via 
	Telegram= 8617397380  
			   OR
	 Email= subhamdwilliams10@yahoo.com                         """)
		if (bu.Label=="New"):
			text=self.choice.GetString(self.choice.GetSelection())
			if (True):
				if(text=='C' or text=='C_With_GTK2' or text=='C_With_GTK3'):
					wildcard = "C Files (*.c)|*.c"
				elif(text=='C++'):
					wildcard = "C++ Files (*.cpp)|*.cpp"
				elif(text=='Python'):
					wildcard = "Python Files (*.py)|*.py"
				elif(text=='Java'):
					wildcard = "Java Files (*.java)|*.java"
				elif(text=='PHP'):
					wildcard = "PHP Files (*.php)|*.php"
				elif(text=='Erlang'):
					wildcard = "Erlang Files (*.erl)|*.erl"
				elif(text=='x86_Assembly'):
					wildcard = "Assembly Files (*.asm)|*.asm"
				elif(text=='Unix_Shell_Script'):
					wildcard = "Shell Script Files (*.sh)|*.sh"	
				elif(text=='Flex'):
					wildcard = "Flex Lexical Files (*.l)|*.l"	
				elif(text=='Rust_Project'):
					wildcard = "Rust Files (*.rs)|*.rs"		
				else:
					wx.MessageBox("You need to chose a file format to forward.....")						
				dlg=wx.FileDialog(self, "Choose a file", os.getcwd(), "", wildcard, wx.FD_OPEN)
				if dlg.ShowModal() == wx.ID_OK:
					path=dlg.GetPath()
			main=wx.BoxSizer(wx.HORIZONTAL)
			b=wx.Button(self,label=path)
			c=wx.Button(self,label="Close")
			main.Add(b,2,wx.EXPAND)
			main.Add(c,0,wx.EXPAND)
			self.mainsizer.Add(main,0,wx.EXPAND|wx.TOP|wx.LEFT|wx.RIGHT,20)
			self.frame.fSizer.Layout()
		if (bu.Label=="Exit"):
			self.parent.Destroy()
		if (bu.Label=="Close"):
			ev=event.EventObject
			self.GetParent().SetSize((0,0))
			ev.GetPrevSibling().Destroy()
			ev.Destroy()
		if(re.search("[/]",bu.Label)):
			#Execute command.............
			if(not(os.path.exists(bu.Label))):
				wx.MessageBox(">>>>>ERROR<<<<< File does not exists...It may have been moved or deleted..Please check the path....")
			else:
				extention=os.path.basename(bu.Label)
				filename=bu.Label.replace("/"+extention,"")
				cd="cd "+filename
				ec="echo \"\n\n\n==========================================================\nPress enter to exit.....\nThank You.........\""
				if (extention.endswith(".py")):
					exe="python3 "+extention
					shell=open("temporaryoshell.sh","w")
					shell.write("#!/bin/bash\n"+cd+"\n"+exe+"\n"+ec+"\nread yo")
					shell.close()	
				elif (extention.endswith(".asm")):
					fil=extention.replace(".asm","")
					exe1="nasm -f elf "+extention
					exe2="ld -m elf_i386 -s -o "+fil+" "+fil+".o"
					exe3="./"+fil
					shell=open("temporaryoshell.sh","w")
					shell.write("#!/bin/bash\n"+cd+"\n"+exe1+"\n"+exe2+"\n"+exe3+"\n"+ec+"\nread yo")
					shell.close()
				elif (extention.endswith(".php")):
					exe="php "+extention
					shell=open("temporaryoshell.sh","w")
					shell.write("#!/bin/bash\n"+cd+"\n"+exe+"\n"+ec+"\nread yo")
					shell.close()
				elif(self.choice.GetString(self.choice.GetSelection())=='C_With_GTK2' and extention.endswith(".c")):
					fil=extention.replace(".c","")
					exe="gcc "+extention+" -o"+fil+" `pkg-config --cflags --libs gtk+-2.0`"
					warn="gcc "+extention+" -o"+fil+" --all-warnings --extra-warnings"
					warn2="echo \"\n\n==========================WARNINGS LOG=======================================\n\""
					shell=open("temporaryoshell.sh","w")
					shell.write("#!/bin/bash\n"+cd+"\n"+exe+"\n"+"./"+fil+"\n"+warn2+"\n"+warn+"\n"+ec+"\nread yo")
					shell.close()
				elif(self.choice.GetString(self.choice.GetSelection())=='C_With_GTK3' and extention.endswith(".c")):
					fil=extention.replace(".c","")
					exe="gcc "+extention+" -o"+fil+" `pkg-config --cflags --libs gtk+-3.0`"
					warn="gcc "+extention+" -o"+fil+" --all-warnings --extra-warnings"
					warn2="echo \"\n\n==========================WARNINGS LOG=======================================\n\""
					shell=open("temporaryoshell.sh","w")
					shell.write("#!/bin/bash\n"+cd+"\n"+exe+"\n"+"./"+fil+"\n"+warn2+"\n"+warn+"\n"+ec+"\nread yo")
					shell.close()		
				elif (extention.endswith(".c")):
					fil=extention.replace(".c","")
					exe="gcc "+extention+" -o"+fil
					warn="gcc "+extention+" -o"+fil+" --all-warnings --extra-warnings"
					warn2="echo \"\n\n==========================WARNINGS LOG=======================================\n\""
					shell=open("temporaryoshell.sh","w")
					shell.write("#!/bin/bash\n"+cd+"\n"+exe+"\n"+"./"+fil+"\n"+warn2+"\n"+warn+"\n"+ec+"\nread yo")
					shell.close()		
				elif (extention.endswith(".cpp")):
					fil=extention.replace(".cpp","")
					exe="g++ "+extention+" -o"+fil
					warn="g++ "+"-W -Wall -Werror "+extention+" -o"+fil
					warn2="echo \"\n\n==========================WARNINGS LOG=======================================\n\""
					shell=open("temporaryoshell.sh","w")
					shell.write("#!/bin/bash\n"+cd+"\n"+exe+"\n"+"./"+fil+"\n"+warn2+"\n"+warn+"\n"+ec+"\nread yo")
					shell.close()
				elif (extention.endswith(".java")):
					fil=extention.replace(".java","")
					exe="javac "+extention
					shell=open("temporaryoshell.sh","w")
					shell.write("#!/bin/bash\n"+cd+"\n"+exe+"\n"+"java "+fil+"\n"+ec+"\nread yo")
					shell.close()	
				elif (extention.endswith(".sh")):
					exe="./"+extention
					shell=open("temporaryoshell.sh","w")
					shell.write("#!/bin/bash\n"+cd+"\n"+"chmod +x "+extention+"\n"+exe+"\n"+ec+"\nread yo")
					shell.close()
				elif (extention.endswith(".l")):
					fil=extention.replace(".l","")
					exe="flex -o "+fil+".yy.c "+extention
					shell=open("temporaryoshell.sh","w")
					shell.write("#!/bin/bash\n"+cd+"\n"+exe+"\n"+"cc "+fil+".yy.c"+" -o "+fil+" -lfl"+"\n"+"./"+fil+"\n"+
					ec+"\nread yo")
					shell.close()
				elif (extention.endswith(".rs")):
					fil=extention.replace(".rs","")
					if (re.search("/(bin)",cd)!=None):
						exe="cargo run --bin "+fil
						shell=open("temporaryoshell.sh","w")
						extra="cd .. && cd .."
						shell.write("#!/bin/bash\n"+cd+"\n"+extra+"\n"+exe+"\n"+ec+"\nread yo")
						shell.close()
					else:
						fil2=re.sub("\/*\.rs","",cd)
						pa=os.path.dirname(os.path.normpath(fil2))
						exe="cargo run --bin "+os.path.basename(pa)
						shell=open("temporaryoshell.sh","w")
						extra="cd .."
						shell.write("#!/bin/bash\n"+cd+"\n"+extra+"\n"+exe+"\n"+ec+"\nread yo")
						shell.close()					
				command="gnome-terminal -e '/bin/sh ./temporaryoshell.sh'"
				os.system("cd "+os.getcwd()+" && chmod 755 temporaryoshell.sh")
				os.system(command)	
				os.system("rm ./temporaryoshell.sh")			 
#    The Actual Frame................		 
class MyFrame(wx.Frame):
	def __init__(self, parent, title="",size=()):
		super().__init__(parent, title=title,size=size)
		self.fSizer = wx.BoxSizer(wx.VERTICAL)
		self.panel = pan(self)
		self.fSizer.Add(self.panel, 0, wx.EXPAND|wx.ALL,20)
		self.fSizer.AddStretchSpacer()
		b3=wx.Button(self,label="Exit")
		self.fSizer.Add(b3,0,wx.ALIGN_RIGHT|wx.ALIGN_BOTTOM|wx.TOP|wx.RIGHT,20)
		b3.Bind(wx.EVT_BUTTON,self.panel.click)
		self.SetSizer(self.fSizer)
		icon = wx.EmptyIcon()
		icon.CopyFromBitmap(wx.Bitmap("/home/sd/Desktop/Cterm_exe/LC2.png", wx.BITMAP_TYPE_ANY))
		self.SetIcon(icon)
		self.Show()
#The Main Application That Will Start The MainLoop.............!!!!!!!!!!!!!!!!!!!		
class MyApp(wx.App):
	def OnInit(self):
		self.frame = MyFrame(None, title="Cterm",size=(800,500))
		self.frame.Show()
		return True
app = MyApp(False)
app.MainLoop()					
		
 

